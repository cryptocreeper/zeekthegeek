#ifndef BASE_TILE_H
#define BASE_TILE_H

#include <vector>
#include "platform_common.h"
#include "level.h"
#include "sprite.h"

using std::vector;

class Level;

class BaseTile {
public:
	static const int kTileSize = 36;
	virtual ~BaseTile();
	virtual void Simulate(Input& input, Level* level, float deltaTime, int row,
						  int col);
	virtual void Draw(int row, int col);
	virtual void PostSimulate(Level* level, int row, int col) {};
	bool ShouldBeDestroyed() { return shouldBeDestroyed; }
	bool IsPostProc() { return postProc; }
protected:
	BaseTile(const char* fileName);
	BaseTile() = default;
	bool shouldBeDestroyed = false;
	bool postProc = false;
	Sprite* sprite;
};

#endif