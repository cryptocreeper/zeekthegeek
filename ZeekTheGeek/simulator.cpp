#include "simulator.h"
#include "movable-tile.h"
#include "zeek.h"

void Simulator::Simulate(Input& input, Level* level, float deltaTime) {
	vector<SimulationStep> steps = {
		// Zeek acts first
		{ [&](int row, int col, BaseTile* t) {
			t->Simulate(input, level, deltaTime, row, col);
		},
			[](BaseTile* t) { return dynamic_cast<Zeek*>(t); }
		}, { // World simulation
			[&](int row, int col, BaseTile* t) {
				t->Simulate(input, level, deltaTime, row, col);
			},
			[](BaseTile* t) { return !dynamic_cast<Zeek*>(t); }
		}, { // Destroy marked tiles
			[&](int row, int col, BaseTile* t) {
				if (t->ShouldBeDestroyed()) level->Destroy(row, col);
			}
		}, { // Update world tiles positions
			[&](int row, int col, BaseTile* t) {
				if (auto mt = dynamic_cast<MovableTile*>(t)) {
					mt->UpdateLevelPos(level, row, col);
				}
			},
			[](BaseTile* t) { return !dynamic_cast<Zeek*>(t); }
		}, { // Update Zeek position
			[&](int row, int col, BaseTile* t) {
				if (auto mt = dynamic_cast<MovableTile*>(t)) {
					mt->UpdateLevelPos(level, row, col);
				}
			},
			[](BaseTile* t) { return dynamic_cast<Zeek*>(t); }
		}, { // Post simulation
			[&](int row, int col, BaseTile* t) {
				t->PostSimulate(level, row, col);
			}
		}
	};

	for (SimulationStep step : steps) PerformSimulStep(level, step);
}

void Simulator::PerformSimulStep(Level* level, SimulationStep step) {
	for (int row = 0; row < kMapH; row++) {
		for (int col = 0; col < kMapW; col++) {
			if (auto tile = dynamic_cast<BaseTile*>(level->GetTile(row, col))) {
				if (!step.predicate || step.predicate(tile)) {
					step.tileAction(row, col, tile);
				}
			}
		}
	}
}
