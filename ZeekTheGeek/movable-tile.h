#ifndef MOVABLE_TILE_H
#define MOVABLE_TILE_H

#include "base_tile.h"
#include "game.h"

class MovableTile : public BaseTile {
public:
	MovableTile() = default;
	virtual void Simulate(Input& input, Level* level, float deltaTime, int row,
						  int col) override;
	virtual void Draw(int row, int col) override;
	virtual void UpdateLevelPos(Level* level, int row, int col);
	virtual void PostSimulate(Level* level, int row, int col) override {};
	float GetShiftPercent();
protected:
	MovableTile(const char* fileName) : BaseTile(fileName) {}
	void UpdateDeltaPos(float deltaTime);

	const float speed = Game::kSpeedRatio / Game::kOneTileMovementTime;
	float xShift, yShift = 0;
	Direction direction = DOWN;
	bool isMoving = false;
	bool blocked = false;
private:
};

#endif