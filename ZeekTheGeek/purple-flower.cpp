#include "purple-flower.h"
#include "renderer.h"
#include "zeek.h"
#include "apple.h"
#include "movable-tile.h"
#include "purple-flower-head.h"

const char* PurpleFlower::kFlowerFile = "assets/purple-flower.png";

PurpleFlower::PurpleFlower(bool isOpened) : BaseTile(kFlowerFile) {
	state = isOpened ? Waiting : Sleeping;
	postProc = true;
	sprite->AddAnim("Sleeping", 0, 1, 1, true);
	sprite->AddAnim("Opening", 1, 4, openingTime / 4, true);
	sprite->AddAnim("Waiting", 2, 1, 1, true);
	sprite->AddAnim("GrabDown", 3, 1, 1, true);
	sprite->AddAnim("GrabLeft", 4, 1, 1, true);
	sprite->AddAnim("GrabRight", 5, 1, 1, true);
	sprite->AddAnim("GrabUp", 6, 1, 1, true);
	sprite->AddAnim("Eating", 7, 4, eatingTime / 4, true);
	sprite->AddAnim("ZeekEating", 8, 6, 0.4 / Game::kSpeedRatio, true);

	if (isOpened) sprite->Play("Waiting");
	else sprite->Play("Sleeping");
}

void PurpleFlower::Simulate(Input& input, Level* level, float deltaTime,
							int row, int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	} else {
		BaseTile::Simulate(input, level, deltaTime, row, col);
	}

	if (state == Eating && zeekEaten) return;
	
	if (state == Sleeping) {
		for (auto dir : { RIGHT, LEFT, UP, DOWN }) {
			if (CheckZeek(level, row, col, dir)) {
				state = Opening;
				sprite->Play("Opening");
			}
		}
	} else if (state == Opening) {
		stateTimer += deltaTime;
		if (stateTimer >= openingTime) {
			stateTimer = 0;
			state = Waiting;
			sprite->Play("Waiting");
		}
	} else if (state == Grabbing) {
		stateTimer += deltaTime;
		if (stateTimer >= grabbingTime) {
			level->Destroy(row, col, direction);
			stateTimer = 0;
			state = Eating;
			if (zeekEaten) sprite->Play("ZeekEating");
			else sprite->Play("Eating");
		}
	} else if (state == Eating) {
		stateTimer += deltaTime;
		if (stateTimer >= eatingTime) {
			stateTimer = 0;
			state = Sleeping;
			sprite->Play("Sleeping");
		}
	}
}

void PurpleFlower::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else BaseTile::Draw(row, col);
}

void PurpleFlower::PostSimulate(Level* level, int row, int col) {
	if (state == Waiting) {
		Direction foodDir;
		if (auto food = FindFoodDirection(level, row, col, foodDir)) {
			int headRow = row, headCol = col;
			int& coordToChange = isHorizontal(foodDir) ? headCol : headRow;
			coordToChange += (isPositive(foodDir) ? 1 : -1);
			zeekEaten = dynamic_cast<Zeek*>(food);
			auto head = new PurpleFlowerHead(foodDir, zeekEaten);
			level->SetTile(headRow, headCol, head);
			direction = foodDir;
			state = Grabbing;

			if (direction == DOWN) sprite->Play("GrabDown");
			else if (direction == LEFT) sprite->Play("GrabLeft");
			else if (direction == RIGHT) sprite->Play("GrabRight");
			else if (direction == UP) sprite->Play("GrabUp");
		}
	}
}

bool PurpleFlower::CheckZeek(Level* level, int row, int col, Direction dir) {
	BaseTile* tile = level->GetNeighborTile(row, col, dir);
	return dynamic_cast<Zeek*>(tile);
}

BaseTile* PurpleFlower::CheckFood(Level* level, int row, int col,
								  Direction dir) {
	BaseTile* tile = level->GetNeighborTile(row, col, dir);
	if (dynamic_cast<Zeek*>(tile) || dynamic_cast<Apple*>(tile)) {
		auto food = dynamic_cast<MovableTile*>(tile);
		return food->GetShiftPercent() <= 0.25 ? food : nullptr;
	}
}

BaseTile* PurpleFlower::FindFoodDirection(Level* level, int row, int col,
										  Direction& foundDir) {
	BaseTile* food = nullptr;
	if (food = CheckFood(level, row, col, RIGHT)) foundDir = RIGHT;
	else if (food = CheckFood(level, row, col, LEFT)) foundDir = LEFT;
	else if (food = CheckFood(level, row, col, UP)) foundDir = UP;
	else if (food = CheckFood(level, row, col, DOWN)) foundDir = DOWN;
	return food;
}