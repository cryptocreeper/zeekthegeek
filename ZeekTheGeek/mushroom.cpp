#include "mushroom.h"
#include "game.h"

const char* Mushroom::kMushroomFile = "assets/mushroom.png";

void Mushroom::Simulate(Input& input, Level* level, float deltaTime,
						int row, int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	CollectableTile::Simulate(input, level, deltaTime, row, col);
	if (CollectionIsFinished()) level->Finish();
}

void Mushroom::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else CollectableTile::Draw(row, col);
}