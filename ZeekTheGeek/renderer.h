#ifndef RENDERER_H
#define RENDERER_H

#include "level.h"
#include "sprite.h"

class Renderer {
public:
	static void Init(const char* title, int width, int height, bool fullScreen);
	static void Destroy();
	static void UpdateWndSize();
	static void RenderLevel(Level* level);
	static void RenderSprite(Sprite* sprite, int row, int col, float xShift,
							 float yShift);
	static SDL_Texture* LoadTexture(const char* fileName);

	Renderer(const Renderer& other) = delete;
	Renderer(Renderer&& other) = delete;
private:
	static SDL_Renderer* sdlRenderer;
	static SDL_Window* sdlWindow;
	static SDL_Rect gameRect;
	static int wndW;
	static int wndH;
	static int origGameW;
	static int origGameH;
	static float scaleFactor;
	static float scaledHorOffset;
	static float scaledVerOffset;
	static float scaledTileSize;

	static void UpdateScalingVals();
};

#endif