#ifndef MUSHROOM_H
#define MUSHROOM_H

#include "collectable-tile.h"
#include "explodable-tile.h"

class Mushroom : public CollectableTile, public ExplodableTile {
public:
	Mushroom() : CollectableTile(kMushroomFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
private:
	static const char* kMushroomFile;
};

#endif