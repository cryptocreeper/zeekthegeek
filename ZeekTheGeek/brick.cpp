#include "brick.h"

const char* Brick::kBrickFile = "assets/brick.png";

Brick::Brick(int brickType) : BaseTile(kBrickFile) {
	sprite->SetIndex(brickType);
}
