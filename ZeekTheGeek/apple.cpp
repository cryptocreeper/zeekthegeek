#include "apple.h"

const char* Apple::kAppleFile = "assets/apple.png";

void Apple::Simulate(Input& input, Level* level, float deltaTime, int row,
					 int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	ItemToMove::Simulate(input, level, deltaTime, row, col);
}

void Apple::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else ItemToMove::Draw(row, col);
}
