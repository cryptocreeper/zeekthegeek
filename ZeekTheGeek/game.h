#ifndef GAME_H
#define GAME_H

#include "SDL.h"
#include "SDL_image.h"
#include "level.h"

class Game {
public:
	static const int kFps;
	static const float kFrameDelay;
	static const float kSpeedRatio;
	static const float kOneTileMovementTime;
	static const float kPFlowerOpeningTime;
	static const float kPFlowerGrabbingTime;
	static const float kPFlowerEatingTime;
	static const float kCrystalActTime;
	static const float kBombActTime;
	static const float kExplosionTime;
	static const float kPoisoningTime;

	Game(const char* title, int width, int height, bool fullScreen);
	~Game();

	void HandleEvents();
	void Update(float deltaTime);
	void Render();
	bool IsRunning() { return isRunning; }
private:
	SDL_Event event;
	bool isRunning;
	int currentLevelNum = 1;
	Level* currentLevel;
	Input input;

	void NextLevel();
	void RestartLevel();
};

#endif