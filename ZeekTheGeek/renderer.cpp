#include "renderer.h"
#include <algorithm>
#include <cmath>
#include "base_tile.h"

int Renderer::wndW;
int Renderer::wndH;
int Renderer::origGameW;
int Renderer::origGameH;
float Renderer::scaleFactor;
float Renderer::scaledHorOffset;
float Renderer::scaledVerOffset;
float Renderer::scaledTileSize;

SDL_Renderer* Renderer::sdlRenderer = nullptr;
SDL_Window* Renderer::sdlWindow = nullptr;
SDL_Rect Renderer::gameRect;

void Renderer::Init(const char* title, int width, int height,
					bool fullScreen) {
	int flags = SDL_WINDOW_RESIZABLE;
	if (fullScreen) flags = SDL_WINDOW_FULLSCREEN;

	sdlWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED,
								 SDL_WINDOWPOS_CENTERED, width, height, flags);
	sdlRenderer = SDL_CreateRenderer(sdlWindow, -1, 0);
	if (sdlRenderer) SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);

	origGameW = width;
	origGameH = height;
	UpdateWndSize();
}

void Renderer::Destroy() {
	SDL_DestroyWindow(sdlWindow);
	SDL_DestroyRenderer(sdlRenderer);
}

void Renderer::UpdateWndSize() {
	SDL_GetWindowSize(Renderer::sdlWindow, &wndW, &wndH);
	UpdateScalingVals();
}

void Renderer::RenderLevel(Level* level) {
	SDL_SetRenderDrawColor(sdlRenderer, 0, 0, 0, 255);
	SDL_RenderClear(sdlRenderer);
	SDL_SetRenderDrawColor(sdlRenderer, 230, 250, 250, 255);
	SDL_RenderFillRect(sdlRenderer, &gameRect);

	for (int row = 0; row < kMapH; row++) {
		for (int col = 0; col < kMapW; col++) {
			BaseTile* tile = nullptr;
			tile = level->GetTile(row, col);
			if (tile != nullptr) {
				tile->Draw(row, col);
			}
		}
	}

	SDL_RenderPresent(sdlRenderer);
}

void Renderer::RenderSprite(Sprite* sprite, int row, int col, float xShift,
							float yShift) {
	using std::round;

	SDL_Rect src = sprite->GetSrcRect();
	SDL_Rect dest = { round(scaledHorOffset + (col + xShift) * scaledTileSize),
				  round(scaledVerOffset + (row + yShift) * scaledTileSize),
				  ceil(scaledTileSize), ceil(scaledTileSize) };
	SDL_RenderCopy(Renderer::sdlRenderer, sprite->GetTexture(), &src, &dest);
}

SDL_Texture* Renderer::LoadTexture(const char* fileName) {
	SDL_Surface* tempSurface = IMG_Load(fileName);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(Renderer::sdlRenderer,
														tempSurface);
	SDL_FreeSurface(tempSurface);
	return texture;
}

void Renderer::UpdateScalingVals() {
	float scaleFactorW = (float)wndW / origGameW;
	float scaleFactorH = (float)wndH / origGameH;
	scaleFactor = scaleFactorW < scaleFactorH ? scaleFactorW : scaleFactorH;
	scaledHorOffset = (wndW - origGameW * scaleFactor) / 2;
	scaledVerOffset = (wndH - origGameH * scaleFactor) / 2;
	scaledTileSize = BaseTile::kTileSize * scaleFactor;

	gameRect.x = (int)std::round(scaledHorOffset);
	gameRect.y = (int)std::round(scaledVerOffset);
	gameRect.w = (int)std::round(origGameW * scaleFactor);
	gameRect.h = (int)std::round(origGameH * scaleFactor);
}