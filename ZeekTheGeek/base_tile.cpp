#include "base_tile.h"
#include "renderer.h"

BaseTile::BaseTile(const char* fileName) {
	sprite = new Sprite(fileName);
}

BaseTile::~BaseTile() {
	delete sprite;
}

void BaseTile::Simulate(Input& input, Level* level, float deltaTime, int row,
						int col) {
	sprite->Update(deltaTime);
}

void BaseTile::Draw(int row, int col) {
	Renderer::RenderSprite(sprite, row, col, 0, 0);
}
