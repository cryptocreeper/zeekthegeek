#include "chest.h"

const char* Chest::kChestFile = "assets/chest.png";

void Chest::Simulate(Input& input, Level* level, float deltaTime, int row,
					 int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	CollectableTile::Simulate(input, level, deltaTime, row, col);
}

void Chest::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else CollectableTile::Draw(row, col);
}