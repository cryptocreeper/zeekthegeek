#ifndef BOMB_H
#define BOMB_H

#include "item-to-move.h"
#include "explodable-tile.h"

class Bomb : public ItemToMove, public ExplodableTile {
public:
	void Simulate(Input& input, Level* level, float deltaTime, int row,
				  int col) override;
	Bomb();
	void Draw(int row,int col) override;
	void Activate();
	bool IsActivated() { return active; }
private:
	static const char* kBombFile;

	bool active = false;
	float actTime = Game::kBombActTime / Game::kSpeedRatio;
	float stateTimer = 0;

	void CheckItemToExplode(Level* level, int row, int col, Direction dir);
};

#endif