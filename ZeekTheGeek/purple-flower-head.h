#ifndef PURPLE_FLOWER_HEAD_H
#define PURPLE_FLOWER_HEAD_H

#include "base_tile.h"

class PurpleFlowerHead : public BaseTile {
public:
	PurpleFlowerHead(Direction dir, bool isZeek);
private:
	static const char* kHeadFile;
};

#endif