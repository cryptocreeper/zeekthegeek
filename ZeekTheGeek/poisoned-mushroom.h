#ifndef POISONED_MUSHROOM_H
#define POISONED_MUSHROOM_H

#include "collectable-tile.h"
#include "explodable-tile.h"

class PoisonedMushroom : public CollectableTile, public ExplodableTile {
public:
	PoisonedMushroom() : CollectableTile(kMushroomFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
private:
	static const char* kMushroomFile;
};

#endif