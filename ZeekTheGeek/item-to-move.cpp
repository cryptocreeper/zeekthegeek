#include "item-to-move.h"

void ItemToMove::Simulate(Input& input, Level* level, float deltaTime, int row,
						  int col) {
	MovableTile::Simulate(input, level, deltaTime, row, col);
}

void ItemToMove::PostSimulate(Level* level, int row, int col) {
	MovableTile::PostSimulate(level, row, col);
}

void ItemToMove::Draw(int row, int col) {
	MovableTile::Draw(row, col);
}

bool ItemToMove::Move(Level* level, int row, int col, Direction dir) {
	if (blocked) return false;

	int& coordToChange = isHorizontal(dir) ? col : row;
	coordToChange += (isPositive(dir) ? 1 : -1);
	if (level->OutOfLevel(row, col)) return false;

	BaseTile* destTile = level->GetTile(row, col);

	if (destTile == nullptr) {
		direction = dir;
		isMoving = true;
	}

	return isMoving;
}
