#ifndef DOOR_H
#define DOOR_H

#include "collectable-tile.h"

class Door : public CollectableTile {
public:
	Door() : CollectableTile(kDoorFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	bool CanCollect(Level* level) override;
private:
	static const char* kDoorFile;
};

#endif