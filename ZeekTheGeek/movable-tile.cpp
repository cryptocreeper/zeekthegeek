#include "movable-tile.h"
#include "renderer.h"
#include <stdlib.h>

void MovableTile::UpdateLevelPos(Level* level, int row, int col) {
	if (!isMoving) return;

	float& shift = isHorizontal(direction) ? xShift : yShift;
	bool positive = isPositive(direction);

	if ((positive && shift >= 1) || (!positive && shift <= -1)) {
		isMoving = false;
		shift = 0;
		level->MoveTo(row, col, direction);
	}
}

float MovableTile::GetShiftPercent() {
	if (!isMoving) return 0;
	float& shift = isHorizontal(direction) ? xShift : yShift;
	return std::abs(shift);
}

void MovableTile::UpdateDeltaPos(float deltaTime) {
	if (!isMoving || blocked) return;
	float& shift = isHorizontal(direction) ? xShift : yShift;
	bool positive = isPositive(direction);
	shift += speed * deltaTime * (positive ? 1 : -1);
}

void MovableTile::Simulate(Input& input, Level* level, float deltaTime, int row,
					 int col) {
	UpdateDeltaPos(deltaTime);
	BaseTile::Simulate(input, level, deltaTime, row, col);
}

void MovableTile::Draw(int row, int col) {
	Renderer::RenderSprite(sprite, row, col, xShift, yShift);
}