#include "bomb.h"
#include "yellow-flower.h"
#include "explodable-tile.h"
#include "renderer.h"

const char* Bomb::kBombFile = "assets/bomb.png";

void Bomb::Simulate(Input& input, Level* level, float deltaTime, int row,
					int col) {
	ItemToMove::Simulate(input, level, deltaTime, row, col);
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
	} else {
		if (!active && isMoving) {
			active = true;
		} else if (active) {
			if (sprite->GetCurrentName() != "Active") sprite->Play("Active");
			stateTimer += deltaTime;
			if (stateTimer >= actTime) {
				exploded = true;

				for (auto dir : {RIGHT, LEFT, UP, DOWN}) {
					CheckItemToExplode(level, row, col, dir);
				}
			}
		}
	}
}

Bomb::Bomb() : ItemToMove(kBombFile) {
	sprite->AddAnim("Inactive", 0, 1, 1, true);
	sprite->AddAnim("Active", 1, 2, actTime / 10, true);
}

void Bomb::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else ItemToMove::Draw(row, col);
}

void Bomb::Activate() {
	active = true;
}

void Bomb::CheckItemToExplode(Level* level, int row, int col, Direction dir) {
	auto tile = level->GetNeighborTile(row, col, dir);
	if (auto bomb = dynamic_cast<Bomb*>(tile)) {
		bomb->Activate();
	} else if (auto explTile = dynamic_cast<ExplodableTile*>(tile)) {
		explTile->Explode();
	}
}
