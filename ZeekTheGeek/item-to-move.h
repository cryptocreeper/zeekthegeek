#ifndef ITEM_TO_MOVE_H
#define ITEM_TO_MOVE_H

#include "movable-tile.h"

class ItemToMove : public MovableTile {
public:
	ItemToMove() = default;
	virtual void Simulate(Input& input, Level* level, float deltaTime, int row,
						  int col) override;
	virtual void PostSimulate(Level* level, int row, int col) override;
	virtual void Draw(int row, int col) override;
	virtual bool Move(Level* level, int row, int col, Direction dir);
protected:
	ItemToMove(const char* fileName) : MovableTile(fileName) {}
};

#endif