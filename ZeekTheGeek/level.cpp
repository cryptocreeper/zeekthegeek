#include "zeek.h"
#include "brick.h"
#include "yellow-flower.h"
#include "apple.h"
#include "mushroom.h"
#include "purple-flower.h"
#include "ball.h"
#include "chest.h"
#include "crystal.h"
#include "bomb.h"
#include "poisoned-mushroom.h"
#include "key.h"
#include "door.h"

Level::Level(int num) {
	for (int row = 0; row < kMapH; row++) {
		for (int col = 0; col < kMapW; col++) {
			int tileId = maps[num - 1][row][col];
			switch (tileId) {
				case 0:
					map[row][col] = nullptr;
					break;
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
				case 8:
				case 9:
				case 10:
				case 11:
					map[row][col] = new Brick(tileId - 1);
					break;
				case 20:
					map[row][col] = new Zeek();
					break;
				case 21:
					map[row][col] = new YellowFlower();
					break;
				case 22:
					map[row][col] = new Mushroom();
					break;
				case 23:
					map[row][col] = new Apple();
					break;
				case 24:
				case 25:
					map[row][col] = new PurpleFlower(tileId == 25);
					break;
				case 26:
					map[row][col] = new Ball();
					break;
				case 27:
					map[row][col] = new Chest();
					break;
				case 28:
					map[row][col] = new Crystal();
					break;
				case 29:
					map[row][col] = new Bomb();
					break;
				case 30:
					map[row][col] = new PoisonedMushroom();
					break;
				case 31:
					map[row][col] = new Key();
					break;
				case 32:
					map[row][col] = new Door();
					break;
				default:
					map[row][col] = nullptr;
					break;
			}
		}
	}
}

Level::~Level() {
	for (int row = 0; row < kMapH; row++) {
		for (int col = 0; col < kMapW; col++) {
			delete map[row][col];
		}
	}
}

BaseTile* Level::GetNeighborTile(int row, int col, Direction dir) {
	int& coordToChange = isHorizontal(dir) ? col : row;
	coordToChange += (isPositive(dir) ? 1 : -1);
	return OutOfLevel(row, col) ? nullptr : map[row][col];
}

void Level::MoveTo(int row, int col, Direction dir) {
	BaseTile* tileToMove = map[row][col];
	map[row][col] = nullptr;
	switch (dir) {
		case UP:
			map[row - 1][col] = tileToMove;
			break;
		case DOWN:
			map[row + 1][col] = tileToMove;
			break;
		case LEFT:
			map[row][col - 1] = tileToMove;
			break;
		case RIGHT:
			map[row][col + 1] = tileToMove;
			break;
	}
}

void Level::Destroy(int row, int col) {
	delete map[row][col];
	map[row][col] = nullptr;
}

void Level::Destroy(int row, int col, Direction dir) {
	int& coordToChange = isHorizontal(dir) ? col : row;
	coordToChange += (isPositive(dir) ? 1 : -1);
	Destroy(row, col);
}

void Level::SetTile(int row, int col, BaseTile* tile) {
	map[row][col] = tile;
}

bool Level::OutOfLevel(int row, int col) {
	return row >= kMapH || row < 0 || col >= kMapW || col < 0;
}
