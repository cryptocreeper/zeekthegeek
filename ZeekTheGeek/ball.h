#ifndef BALL_H
#define BALL_H

#include "item-to-move.h"

class Ball : public ItemToMove {
public:
	Ball() : ItemToMove(kBallFile) {}
private:
	static const char* kBallFile;
};

#endif