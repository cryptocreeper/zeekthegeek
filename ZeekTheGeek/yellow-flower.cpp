#include "yellow-flower.h"

const char* YellowFlower::kFlowerFile = "assets/yellow-flower.png";

void YellowFlower::Simulate(Input& input, Level* level, float deltaTime,
							int row, int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	CollectableTile::Simulate(input, level, deltaTime, row, col);
}

void YellowFlower::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else CollectableTile::Draw(row, col);
}