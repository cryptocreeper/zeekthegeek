#ifndef SPRITE_H
#define SPRITE_H

#include "SDL.h"
#include "SDL_image.h"
#include <map>

struct Animation {
	int index;
	int frames;
	float speed;
	bool loop;

	Animation() {}

	Animation(int i, int f, float s, bool l) {
		index = i;
		frames = f;
		speed = s;
		loop = l;
	}
};

class Sprite {
public:
	Sprite(const char* path);
	~Sprite();

	void AddAnim(const char* name, int index, int frames, float speed,
				 bool loop);
	void Update(float deltaTime);
	void Play(const char* name);

	void SetIndex(int index) { this->index = index; }
	SDL_Rect GetSrcRect() { return srcRect; }
	SDL_Texture* GetTexture() { return texture; }
	const char* GetCurrentName() { return curName; }
private:
	bool animated = false;
	SDL_Texture* texture;
	std::map<const char*, Animation> animations;
	SDL_Rect srcRect = { 0, 0, 36, 36 };
	float timer = 0;
	const char* curName;
	int index = 0;
	int frames;
	float speed;
	bool loop;
};

#endif
