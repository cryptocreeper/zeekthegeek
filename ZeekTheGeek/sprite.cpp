#include "sprite.h"
#include "renderer.h"

Sprite::Sprite(const char* path) {
	texture = Renderer::LoadTexture(path);
}

Sprite::~Sprite() {
	SDL_DestroyTexture(texture);
}

void Sprite::AddAnim(const char* name, int index, int frames, float speed,
					 bool loop) {
	animated = true;
	animations.emplace(name, Animation(index, frames, speed, loop));
	if (animations.size() == 1) Play(name);
}

void Sprite::Update(float deltaTime) {
	if (animated) {
		timer += deltaTime;

		srcRect.x = ((int)(timer / speed) % frames) * srcRect.w;
	}

	srcRect.y = index * srcRect.h;
}

void Sprite::Play(const char* name) {
	timer = 0;
	curName = name;
	frames = animations[name].frames;
	index = animations[name].index;
	speed = animations[name].speed;
	loop = animations[name].loop;
}
