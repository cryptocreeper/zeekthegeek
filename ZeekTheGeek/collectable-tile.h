#ifndef COLLECTABLE_TILE_H
#define COLLECTABLE_TILE_H

#include "base_tile.h"
#include "game.h"

class CollectableTile : public BaseTile {
public:
	~CollectableTile();
	virtual void Simulate(Input& input, Level* level, float deltaTime, int row,
						  int col) override;
	virtual void Draw(int row, int col) override;
	void Collect() { collected = true; }
	virtual bool CanCollect(Level* level) { return true; };
protected:
	CollectableTile(const char* fileName);
	bool CollectionIsFinished() { return collTimer >= collTime; }
	bool collected = false;
private:
	static const char* kCollectionFile;

	Sprite* collSprite;
	float collTime = Game::kExplosionTime / Game::kSpeedRatio;
	float collTimer = 0;
};

#endif