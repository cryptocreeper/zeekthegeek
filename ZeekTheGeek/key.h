#ifndef KEY_H
#define KEY_H

#include "collectable-tile.h"

class Key : public CollectableTile {
public:
	Key() : CollectableTile(kKeyFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	bool CanCollect(Level* level) override;
private:
	static const char* kKeyFile;
};

#endif