#ifndef ZEEK_H
#define ZEEK_H

#include "base_tile.h"
#include "platform_common.h"
#include "game.h"
#include "movable-tile.h"
#include "explodable-tile.h"

class Zeek : public MovableTile, public ExplodableTile {
public:
	Zeek();
	~Zeek() {};
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
	bool IsMoving() { return isMoving; }
	void Poison() { poisoned = true; }
private:
	static const char* kZeekFile;

	bool poisoned = false;
	float poisTime = Game::kPoisoningTime / Game::kSpeedRatio;
	float poisTimer = 0;

	void ProcessDesiredDirection(Level* level, int row, int col);
	void ChangeAnimation(const char* animName);
	void UpdateAnimation(float deltaTime);
};

#endif