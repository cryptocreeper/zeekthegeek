#include "purple-flower-head.h"

const char* PurpleFlowerHead::kHeadFile = "assets/purple-flower-head.png";

PurpleFlowerHead::PurpleFlowerHead(Direction dir, bool isZeek)
	: BaseTile(kHeadFile) {
	if (dir == RIGHT) sprite->SetIndex(isZeek ? 1 : 0);
	else if (dir == LEFT) sprite->SetIndex(isZeek ? 3 : 2);
	else if (dir == UP) sprite->SetIndex(isZeek ? 5 : 4);
	else if (dir == DOWN) sprite->SetIndex(isZeek ? 7 : 6);
}
