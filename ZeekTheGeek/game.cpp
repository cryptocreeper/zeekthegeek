#include "game.h"
#include "renderer.h"
#include <iostream>
#include "simulator.h"

const int Game::kFps = 60;
const float Game::kFrameDelay = 1000.0 / kFps;
const float Game::kSpeedRatio = 1.6;
const float Game::kOneTileMovementTime = 0.88;
const float Game::kPFlowerOpeningTime = 0.55;
const float Game::kPFlowerGrabbingTime = 1.6;
const float Game::kPFlowerEatingTime = 13.4;
const float Game::kCrystalActTime = 4.3;
const float Game::kBombActTime = 13.3;
const float Game::kExplosionTime = 0.6;
const float Game::kPoisoningTime = 4;

Game::Game(const char* title, int width, int height, bool fullScreen) {
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) return;

	Renderer::Init(title, width, height, fullScreen);
	currentLevel = new Level(currentLevelNum);
	isRunning = true;
}

Game::~Game() {
	Renderer::Destroy();
	SDL_Quit();
}

void Game::HandleEvents() {
	SDL_PollEvent(&event);

	switch (event.type) {
		case SDL_WINDOWEVENT:
			switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					Renderer::UpdateWndSize();
					break;
			}
			break;
		case SDL_QUIT:
			isRunning = false;
			break;
	}
}

#define processButton(b, key)\
case key: {\
input.buttons[b].changed = isDown != input.buttons[b].isDown;\
input.buttons[b].isDown = isDown;\
} break;

void Game::Update(float deltaTime) {
	auto eventType = event.type;
	if (eventType == SDL_KEYDOWN || eventType == SDL_KEYUP) {
		bool isDown = eventType == SDL_KEYDOWN;
		switch (event.key.keysym.sym) {
			processButton(BUTTON_W, SDLK_w);
			processButton(BUTTON_A, SDLK_a);
			processButton(BUTTON_S, SDLK_s);
			processButton(BUTTON_D, SDLK_d);
			processButton(BUTTON_ENTER, SDLK_RETURN);
		}
	}

	if (pressed(BUTTON_ENTER)) RestartLevel();
	if (currentLevel->IsFinished()) {
		for (int i = 0; i < BUTTON_COUNT; i++) {
			input.buttons[i].isDown = false;
			input.buttons[i].changed = false;
		}
		NextLevel();
	}

	Simulator::Simulate(input, currentLevel, deltaTime);
}

void Game::Render() {
	Renderer::RenderLevel(currentLevel);
}

void Game::NextLevel() {
	currentLevelNum++;
	delete currentLevel;
	currentLevel = new Level(currentLevelNum);
}

void Game::RestartLevel() {
	delete currentLevel;
	currentLevel = new Level(currentLevelNum);
}