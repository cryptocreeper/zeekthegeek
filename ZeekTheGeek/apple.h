#ifndef APPLES_H
#define APPLES_H

#include "item-to-move.h"
#include "explodable-tile.h"

class Apple : public ItemToMove, public ExplodableTile {
public:
	Apple() : ItemToMove(kAppleFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	virtual void Draw(int row, int col) override;
private:
	static const char* kAppleFile;
};

#endif