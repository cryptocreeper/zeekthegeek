#include "crystal.h"

const char* Crystal::kCrystalFile = "assets/crystal.png";

Crystal::Crystal() : ItemToMove(kCrystalFile) {
	sprite->AddAnim("Inactive", 0, 1, 1, true);
	sprite->AddAnim("Active", 1, 2, actTime / 10, true);
	sprite->AddAnim("Burst", 2, 3, burstTime / 3, true);
}

void Crystal::Simulate(Input& input, Level* level, float deltaTime, int row, int col) {
	ItemToMove::Simulate(input, level, deltaTime, row, col);

	if (state == Active) {
		stateTimer += deltaTime;
		if (stateTimer >= actTime) {
			stateTimer = 0;
			state = Burst;
			sprite->Play("Burst");
		}
	}

	if (state == Burst) {
		stateTimer += deltaTime;
		if (stateTimer >= burstTime) shouldBeDestroyed = true;
	}
}

void Crystal::PostSimulate(Level* level, int row, int col) {
	if (state == Inactive) {
		for (auto dir : { RIGHT, LEFT, UP, DOWN }) {
			ActivateNearCrystal(level, row, col, dir);
		}
	}
}

void Crystal::Draw(int row, int col) {
	ItemToMove::Draw(row, col);
}

void Crystal::Activate() {
	if (state == Inactive) {
		state = Active;
		blocked = true;
		sprite->Play("Active");
	}
}

void Crystal::ActivateNearCrystal(Level* level, int row, int col,
								  Direction dir) {
	if (auto nearCrystal =
		dynamic_cast<Crystal*>(level->GetNeighborTile(row, col, dir))) {
		Activate();
		nearCrystal->Activate();
	}
}
