#ifndef CHEST_H
#define CHEST_H

#include "collectable-tile.h"
#include "explodable-tile.h"

class Chest : public CollectableTile, public ExplodableTile {
public:
	Chest() : CollectableTile(kChestFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
private:
	static const char* kChestFile;
};

#endif
