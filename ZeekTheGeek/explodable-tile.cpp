#include "explodable-tile.h"
#include "renderer.h"

const char* ExplodableTile::kExplodableFile = "assets/explosion.png";

ExplodableTile::ExplodableTile() {
	explSprite = new Sprite(kExplodableFile);
	explSprite->AddAnim("Explosion", 0, 3, explTime / 3, false);
}

ExplodableTile::~ExplodableTile() {
	SDL_Log("Expl destr");
	delete explSprite;
}

void ExplodableTile::Simulate(float deltaTime) {
	explTimer += deltaTime;
	explSprite->Update(deltaTime);
}

void ExplodableTile::Draw(int row, int col) {
	Renderer::RenderSprite(explSprite, row, col, 0, 0);
}
