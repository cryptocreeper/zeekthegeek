#include "key.h"

const char* Key::kKeyFile = "assets/key.png";

void Key::Simulate(Input& input, Level* level, float deltaTime, int row,
				   int col) {
	if (collected) level->SetKeyCollected(true);
	CollectableTile::Simulate(input, level, deltaTime, row, col);
}

bool Key::CanCollect(Level* level) {
	return !level->IsKeyCollected();
}
