#include "collectable-tile.h"
#include "renderer.h"

const char* CollectableTile::kCollectionFile = "assets/collection.png";

CollectableTile::CollectableTile(const char* fileName) : BaseTile(fileName) {
	collSprite = new Sprite(kCollectionFile);
	collSprite->AddAnim("Collection", 0, 3, collTime / 3, false);
}

CollectableTile::~CollectableTile() {
	delete collSprite;
}

void CollectableTile::Simulate(Input& input, Level* level, float deltaTime,
							   int row, int col) {
	if (!collected) return;

	collTimer += deltaTime;
	collSprite->Update(deltaTime);
	if (CollectionIsFinished()) shouldBeDestroyed = true;
}

void CollectableTile::Draw(int row, int col) {
	if (collected) Renderer::RenderSprite(collSprite, row, col, 0, 0);
	else BaseTile::Draw(row, col);
}


