#include "game.h"
#include "renderer.h"

int main(int argc, char* argv[]) {
	Uint32 frameStart;
	float deltaTime = Game::kFrameDelay;

	Game* game = new Game("Title", BaseTile::kTileSize * kMapW,
						  BaseTile::kTileSize * kMapH, false);

	while (game->IsRunning()) {
		frameStart = SDL_GetTicks();

		game->HandleEvents();
		game->Update(deltaTime);
		game->Render();

		deltaTime = (SDL_GetTicks() - frameStart) / 1000.0f;
	}

	delete game;

	return 0;
}