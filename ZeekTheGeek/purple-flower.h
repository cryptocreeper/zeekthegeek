#ifndef PURPLE_FLOWER_H
#define PURPLE_FLOWER_H

#include "base_tile.h"
#include "game.h"
#include "explodable-tile.h"

class PurpleFlower : public BaseTile, public ExplodableTile {
public:
	PurpleFlower(bool isOpened);
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
	virtual void PostSimulate(Level* level, int row, int col) override;
private:
	static const char* kFlowerFile;

	enum State { Sleeping, Opening, Waiting, Grabbing, Eating };

	const float openingTime = Game::kPFlowerOpeningTime / Game::kSpeedRatio;
	const float grabbingTime = Game::kPFlowerGrabbingTime / Game::kSpeedRatio;
	const float eatingTime = Game::kPFlowerEatingTime / Game::kSpeedRatio;

	State state;
	Direction direction;
	float stateTimer = 0;
	bool zeekEaten = false;

	bool CheckZeek(Level* level, int row, int col, Direction dir);
	BaseTile* CheckFood(Level* level, int row, int col, Direction dir);
	BaseTile* FindFoodDirection(Level* level, int row, int col, Direction& foundDir);
};

#endif