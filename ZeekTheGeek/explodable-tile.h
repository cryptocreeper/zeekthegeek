#ifndef EXPLODABLE_TILE_H
#define EXPLODABLE_TILE_H

#include <windows.h>
#include <gdiplus.h>
#include "level.h"
#include <vector>
#include "game.h"
#include "sprite.h"

using Gdiplus::Bitmap;
using std::vector;

class ExplodableTile {
public:
	ExplodableTile();
	~ExplodableTile();

	virtual void Explode() { exploded = true; }
	void Simulate(float deltaTime);
	void Draw(int row, int col);
protected:
	Sprite* explSprite;
	bool exploded = false;
	float explTime = Game::kExplosionTime / Game::kSpeedRatio;
	float explTimer = 0;
private:
	static const char* kExplodableFile;
};

#endif