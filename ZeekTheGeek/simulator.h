#ifndef SIMULATOR_H
#define SIMULATOR_H

#include <functional>
#include <vector>
#include "level.h"
#include "platform_common.h"
#include "brick.h"
#include <utility>

using std::function;
using std::vector;

struct SimulationStep {
	function<void(int, int, BaseTile*)> tileAction;
	function<bool(BaseTile* t)> predicate = nullptr;
};

class Simulator {
public:
	static void Simulate(Input& input, Level* level, float deltaTime);
private:
	static void PerformSimulStep(Level* level, SimulationStep step);
};

#endif