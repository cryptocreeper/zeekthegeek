#ifndef CRYSTAL_H
#define CRYSTAL_H

#include "item-to-move.h"

class Crystal : public ItemToMove {
public:
	void Simulate(Input& input, Level* level, float deltaTime, int row,
				  int col) override;
	Crystal();
	virtual void PostSimulate(Level* level, int row, int col) override;
	void Draw(int row, int col) override;
	void Activate();
	bool IsActivated() { return state == Active; }
private:
	static const char* kCrystalFile;

	enum State { Inactive, Active, Burst };

	State state = Inactive;
	float burstTime = Game::kExplosionTime / Game::kSpeedRatio;
	float actTime = Game::kCrystalActTime / Game::kSpeedRatio;
	float stateTimer = 0;

	void ActivateNearCrystal(Level* level, int row, int col, Direction dir);
};

#endif