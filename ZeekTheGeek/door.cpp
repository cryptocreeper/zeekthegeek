#include "door.h"

const char* Door::kDoorFile = "assets/door.png";

void Door::Simulate(Input& input, Level* level, float deltaTime, int row,
					int col) {
	if (collected) level->SetKeyCollected(false);
	CollectableTile::Simulate(input, level, deltaTime, row, col);
}

bool Door::CanCollect(Level* level) {
	return level->IsKeyCollected();
}