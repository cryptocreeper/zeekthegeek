#include "poisoned-mushroom.h"

const char* PoisonedMushroom::kMushroomFile = "assets/poisoned-mushroom.png";

void PoisonedMushroom::Simulate(Input& input, Level* level, float deltaTime,
								int row, int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	CollectableTile::Simulate(input, level, deltaTime, row, col);
}

void PoisonedMushroom::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else CollectableTile::Draw(row, col);
}