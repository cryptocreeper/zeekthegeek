#include "zeek.h"
#include "yellow-flower.h"
#include "renderer.h"
#include <string>
#include "item-to-move.h"
#include "crystal.h"
#include "poisoned-mushroom.h"

const char* Zeek::kZeekFile = "assets/zeek.png";

Zeek::Zeek() : MovableTile(kZeekFile) {
	float moveAnmSpeed = Game::kOneTileMovementTime / (4 * Game::kSpeedRatio);
	sprite->AddAnim("Idle", 0, 4, moveAnmSpeed * 2, true);
	sprite->AddAnim("Down", 1, 4, moveAnmSpeed, true);
	sprite->AddAnim("Right", 2, 4, moveAnmSpeed, true);
	sprite->AddAnim("Up", 3, 4, moveAnmSpeed, true);
	sprite->AddAnim("Left", 4, 4, moveAnmSpeed, true);
	sprite->AddAnim("Poison", 5, 2, 0.2, true);
}

void Zeek::ProcessDesiredDirection(Level* level, int row, int col) {
	int& coordToChange = isHorizontal(direction) ? col : row;
	coordToChange += (isPositive(direction) ? 1 : -1);
	if (level->OutOfLevel(row, col)) return;

	BaseTile* destTile = level->GetTile(row, col);

	if (destTile == nullptr) isMoving = true;
	else if (auto colTile = dynamic_cast<CollectableTile*>(destTile)) {
		if (colTile->CanCollect(level)) {
			colTile->Collect();
			isMoving = true;
			poisoned = dynamic_cast<PoisonedMushroom*>(destTile);
		}
	} else if (auto item = dynamic_cast<ItemToMove*>(destTile)) {
		isMoving = item->Move(level, row, col, direction);
	}
}

void Zeek::ChangeAnimation(const char* animName) {
	if (sprite->GetCurrentName() != animName) {
		sprite->Play(animName);
	}
}

void Zeek::UpdateAnimation(float deltaTime) {
	if (isMoving) {
		switch (direction) {
			case LEFT:
				ChangeAnimation("Left");
				break;
			case RIGHT:
				ChangeAnimation("Right");
				break;
			case UP:
				ChangeAnimation("Up");
				break;
			case DOWN:
				ChangeAnimation("Down");
				break;
		}
	} else if (poisoned) {
		ChangeAnimation("Poison");
	} else {
		ChangeAnimation("Idle");
	}
}

void Zeek::Simulate(Input& input, Level* level, float deltaTime, int row,
					int col) {
	if (exploded) {
		ExplodableTile::Simulate(deltaTime);
		if (explTimer >= explTime) shouldBeDestroyed = true;
		return;
	}

	if (!isMoving && poisoned) {
		poisTimer += deltaTime;
		if (poisTimer >= poisTime) exploded = true;
	}

	if (!isMoving && !poisoned) {
		if (isDown(BUTTON_D) || isDown(BUTTON_A) || isDown(BUTTON_W)
			|| isDown(BUTTON_S)) {
			if (isDown(BUTTON_D)) direction = RIGHT;
			else if (isDown(BUTTON_A)) direction = LEFT;
			else if (isDown(BUTTON_W)) direction = UP;
			else if (isDown(BUTTON_S)) direction = DOWN;
			ProcessDesiredDirection(level, row, col);
		}
	}

	UpdateAnimation(deltaTime);
	MovableTile::Simulate(input, level, deltaTime, row, col);
}

void Zeek::Draw(int row, int col) {
	if (exploded) ExplodableTile::Draw(row, col);
	else MovableTile::Draw(row, col);
}
