#ifndef BRICK_H
#define BRICK_H

#include "base_tile.h"

class Brick : public BaseTile {
public:
	Brick(int brickType);
private:
	static const char* kBrickFile;
};

#endif