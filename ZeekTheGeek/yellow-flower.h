#ifndef YELLOW_FLOWER_H
#define YELLOW_FLOWER_H

#include "collectable-tile.h"
#include "explodable-tile.h"

class YellowFlower : public CollectableTile, public ExplodableTile {
public:
	YellowFlower() : CollectableTile(kFlowerFile) {}
	void Simulate(Input& input, Level* level, float deltaTime, int row, int col)
		override;
	void Draw(int row, int col) override;
private:
	static const char* kFlowerFile;
};

#endif