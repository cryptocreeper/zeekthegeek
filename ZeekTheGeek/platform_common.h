#ifndef PLATFORM_COMMON_H
#define PLATFORM_COMMON_H

#define isDown(b) input.buttons[b].isDown
#define pressed(b) (input.buttons[b].isDown && input.buttons[b].changed)
#define released(b) (!input.buttons[b].isDown && input.buttons[b].changed)

struct ButtonState {
	bool isDown;
	bool changed;
};

enum {
	BUTTON_W,
	BUTTON_A,
	BUTTON_S,
	BUTTON_D,
	BUTTON_ENTER,

	BUTTON_COUNT // Should be the last item
};

struct Input {
	ButtonState buttons[BUTTON_COUNT];
};

#endif