# Description
Remake of the old 2D puzzle game [Zeek The Geek: Part 1](https://www.old-games.ru/game/download/5034.html)

# Development settings
## Visual Studio configuration
1. Download **SDL2** [here](https://www.libsdl.org/download-2.0.php) (Development Libraries);
2. Copy **SDL2-2.0.14\lib\x86\SDL2.dll** to folder with the sources;
3. Download **SDL_image 2.0** [here](https://www.libsdl.org/projects/SDL_image/) (Development Libraries);
4. Copy **libpng16-16.dll**, **SDL2_image.dll**, **zlib1.dll** from **SDL2_image-2.0.5\lib\x86\\** to folder with the sources;
5. Put **SDL2-2.0.14** and **SDL2_image-2.0.5** folders to some common place, for example: **D:\Repos\Cpp\Libs\\**;
6. In Visual Studio right click on project folder -> **Properties**;
7. **VC++ Directories** -> **Include Directories** -> **<Edit...>**;
8. Add 2 new lines: **D:\Repos\Cpp\Libs\SDL2-2.0.14\include** and **D:\Repos\Cpp\Libs\SDL2_image-2.0.5\include**;
9. **VC++ Directories** -> **Library Directories** -> **<Edit...>**;
10. Add 2 new lines: **D:\Repos\Cpp\Libs\SDL2-2.0.14\lib\x86** and **D:\Repos\Cpp\Libs\SDL2_image-2.0.5\lib\x86**;
11. **C/C++** -> **Additional Include Directories** -> **<Edit...>**;
12. Add 2 new lines: **D:\Repos\Cpp\Libs\SDL2-2.0.14\include** and **D:\Repos\Cpp\Libs\SDL2_image-2.0.5\include**;
13. **Linker** -> **General** -> **Additional Library Directories** -> **<Edit...>**;
14. Add 2 new lines: **D:\Repos\Cpp\Libs\SDL2-2.0.14\lib\x86** and **D:\Repos\Cpp\Libs\SDL2_image-2.0.5\lib\x86**;
15. **Linker** -> **Input** -> **Additional Dependencies** -> **<Edit...>**;
16. Add 3 lines: **SDL2.lib**, **SDL2main.lib**, **SDL2_image.lib**;
17. Click **OK** to save all settings;
18. Select **x86** in Solution Platforms.